<?php

use Illuminate\Support\Facades\Route;

Route::controller(\App\Http\Controllers\SecurityController::class)->group(function (){

    Route::get("/login", function (){ return view('pages/login'); })->name('login');
    Route::post("/login", 'login');

    Route::get("/register", function (){ return view('pages/register'); })->name('register');
    Route::post("/register", 'register');

    Route::get("/logout", 'logout');
});

Route::middleware(['auth:sanctum', 'role:user'])->group(function (){

    Route::controller(\App\Http\Controllers\CartController::class)->group(function (){
        Route::get("/cart", 'index');
        Route::post("/cart/product/{product}", 'storeProduct');
        Route::delete("/cart/product/{product}", 'deleteProduct');
        Route::patch("/cart/product/increase/{product}", 'increaseProductCount');
        Route::patch("/cart/product/decrease/{product}", 'decreaseProductCount');
    });

    Route::controller(\App\Http\Controllers\OrderController::class)->group(function (){
        Route::post("/order/new", 'store');
    });

});

Route::middleware(['auth:sanctum', 'role:admin'])->group(function (){

    Route::controller(\App\Http\Controllers\ProductController::class)->group(function (){
        Route::get("/admin/products", 'index');
        Route::get("/admin/products/new", 'store');
        Route::post("/admin/products/new", 'store');
        Route::get("/admin/products/{product}", 'show');
        Route::patch("/admin/products/{product}", 'update');
        Route::delete("/admin/products/{product}", 'destroy');
    });

    Route::controller(\App\Http\Controllers\CategoryController::class)->group(function (){
        Route::get("/admin/categories", 'index');
        Route::get("/admin/categories/new", 'store');
        Route::post("/admin/categories/new", 'store');
        Route::get("/admin/categories/{category}", 'show');
        Route::patch("/admin/categories/{category}", 'update');
        Route::delete("/admin/categories/{category}", 'destroy');
    });

    Route::controller(\App\Http\Controllers\OrderController::class)->group(function (){
        Route::get("/admin/orders", 'index');
        Route::get("/admin/orders/{order}", 'show');
        Route::patch("/admin/orders/{order}", 'update');
        Route::delete("/admin/orders/{order}", 'destroy');
    });

    Route::controller(\App\Http\Controllers\AdminController::class)->group(function (){
        Route::get("/admin", 'index');
    });
});

Route::controller(\App\Http\Controllers\HomeController::class)->group(function (){
    Route::get("/", 'home')->name('home');
    Route::get("/catalog", 'catalog')->name('catalog');
    Route::get("/catalog/product/{product}", 'product');
});
