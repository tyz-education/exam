<?php

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;

trait JsonResponseTrait
{
    /**
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function failed(array $data = [], int $code = 200): JsonResponse
    {
        return $this->makeResponse(false, $data, $code);
    }

    /**
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function success(array $data = [], int $code = 200): JsonResponse
    {
        return $this->makeResponse(true, $data, $code);
    }

    /**
     * @param bool $success
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    private function makeResponse(bool $success = true, array $data = [], int $code = 200): JsonResponse
    {
        $data = [...['success' => $success], ...$data];

        return new JsonResponse($data, $code);
    }
}
