<?php

namespace App\Http\Controllers;

use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(){

        $cart = Auth::user()->cart;

        return view('pages.cart', compact('cart'));
    }

    public function storeProduct(Product $product)
    {
        $this->storeCartItem($this->findCartItem($product), $product);

        return $this->success();
    }

    public function deleteProduct(Product $product)
    {
        $cartItem = $this->findCartItem($product);

        if (!$cartItem) return $this->failed();

        $cartItem->delete();

        return redirect('/cart');
    }

    public function increaseProductCount(Product $product)
    {
        $cartItem = $this->findCartItem($product);

        if (!$cartItem) return $this->failed();

        if ($product->count > $cartItem->count){

            $cartItem->count += 1;

            $cartItem->save();

            $data = ['count' => $cartItem->count];
        }
        else{
            $data = ['count' => $cartItem->count, 'error' => "this product has only " . $cartItem->count . " items"];
        }

        return $this->success($data);
    }

    public function decreaseProductCount(Product $product)
    {
        $cartItem = $this->findCartItem($product);

        if (!$cartItem) return $this->failed();

        if ($cartItem->count > 0){

            $cartItem->count -= 1;

            $cartItem->save();

            $data = ['count' => $cartItem->count];
        }
        else{
            $data = ['count' => 0,'error' => "count of products must be more than 0"];
        }

        return $this->success($data);
    }

    public function findCartItem($product)
    {
        $cart = auth()->user()->cart()->first();

        return $cart->items()->where('product_id', $product->id)->first();
    }

    public function storeCartItem(?CartItem $cartItem, Product $product): CartItem
    {
        $cartItem = $cartItem ?: new CartItem();

        $cart = auth()->user()->cart()->first();

        $cartItem->count = 1;

        $cartItem->cart_id = $cart->id;

        $cartItem->product_id = $product->id;

        $cartItem->save();

        return $cartItem;
    }
}
