<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();

        return view('admin/orders', compact('orders'));
    }

    public function show(Order $order)
    {
        return view('admin/order', compact('order'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $user->getAuthPassword();

        if(!Hash::check($request->password, $user->getAuthPassword())){
            return throw ValidationException::withMessages(['password is incorrect']);
        }

        $order = new Order;

        $order->user_id = $user->id;
        $order->status = 'Новый';

        $order->save();

        foreach ($user->cart->items as $cartItem){

            $orderItem = new OrderItem();

            $orderItem->count = $cartItem->count;
            $orderItem->product_id = $cartItem->product->id;
            $orderItem->order_id = $order->id;

            $orderItem->save();

            $cartItem->product->count -= $cartItem->count;
            $cartItem->product->save();

            $cartItem->delete();
        }

        return $this->success();
    }

    public function update(Request $request, Order $order)
    {
        $request->validate([
            'status' => ['required', 'string'],
        ]);

        $order->status = $request->status;

        $order->save();

        return redirect('/admin/orders');
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect('/admin/orders');
    }
}
