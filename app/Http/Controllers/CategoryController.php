<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('admin/categories', compact('categories'));
    }

    public function store(Request $request)
    {
        $category = new Category();

        if ($request->isMethod('GET')){
            return view('admin/category', compact('category'));
        }

        return $this->newOrUpdateCategory($request, $category);
    }

    public function show(Category $category)
    {
        return view('admin/category', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        return $this->newOrUpdateCategory($request, $category);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect('/admin/categories');
    }

    private function newOrUpdateCategory(Request $request, Category $category){

        $request->validate([
            'name' => ['required', 'string', 'min:2'],
        ]);

        $category->name = $request->name;

        $category->save();

        return redirect('/admin/categories');
    }
}
