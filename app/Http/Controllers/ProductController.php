<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('admin/products', compact('products'));
    }

    public function store(Request $request)
    {
        $product = new Product();
        $categories = Category::all();

        if ($request->isMethod('GET')){
            return view('admin/product', compact('product', 'categories'));
        }

        return $this->newOrUpdateProduct($request, $product);
    }

    public function show(Product $product)
    {
        $categories = Category::all()->except($product->category->id);

        return view('admin/product', compact('product', 'categories'));
    }

    public function update(Request $request, Product $product)
    {
        return $this->newOrUpdateProduct($request, $product);
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect('/admin/products');
    }

    private function newOrUpdateProduct(Request $request, Product $product){

        $request->validate([
            'name' => ['required', 'string', 'min:2'],
            'price' => ['required', 'integer'],
            'count' => ['required', 'integer'],
            'country' => ['required', 'string', 'min:2'],
            'year' => ['required', 'date'],
            'model' => ['required', 'string'],
            'category_id' => ['required', 'integer'],
            'photo' => ['file'],
        ]);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->count = $request->count;
        $product->country = $request->country;
        $product->year = $request->year;
        $product->model = $request->model;
        $product->publish = $request->publish ? true : false;
        $product->category_id = $request->category_id;

        if ($request->photo) {
            $imageName = md5($request->photo) . "." . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path("photos"), $imageName);
            $product->photo = "/photos/$imageName";
        }

        $product->save();

        return redirect('/admin/products');
    }
}
