<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function home(){

        return view('base');
    }

    public function catalog(){

        $products = Product::where('publish', 1)->where('count', '>', 0)->get();

        return view('pages.catalog', compact('products'));
    }

    public function product(Product $product){

        return view('pages.product', compact('product'));
    }
}
