<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= Category::count(); $i++){

            for ($j = 0; $j <= 7; $j++){

                Product::create([
                    'name' => Str::random(20),
                    'price' => mt_rand(1,20000),
                    'count' => mt_rand(1,10),
                    'country' => Str::random(20),
                    'year' => new \DateTime(),
                    'model' => Str::random(20),
                    'category_id' => $i,
                    'publish' => true,
                    'photo' => '/photos/product.jpg',
                ]);
            }
        }
    }
}
