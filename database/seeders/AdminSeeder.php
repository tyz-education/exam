<?php

namespace Database\Seeders;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'surname' => 'admin',
            'patronymic' => 'admin',
            'email' => 'admin@mail.ru',
            'password' => Hash::make('admin'),
            'login' => 'admin',
            'roles' => ['admin', 'user'],
        ]);
    }
}
