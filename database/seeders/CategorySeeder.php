<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['лазерные принтеры', 'струйные принтеры', 'термопринтеры'];

        foreach ($categories as $categoryName){

            Category::create([
                'name' => $categoryName,
            ]);

        }
    }
}
