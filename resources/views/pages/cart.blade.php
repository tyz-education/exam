@extends('base')

@section('main')

    @parent

    <div class="form flex-column">

        @foreach($cart->items as $item)

            <div class="cart_item _parent" data-product-id="{{ $item->product->id }}">
                <div class="product__name">{{ $item->product->name }}</div>

                <div class="count">Количество в корзине:
                    <button class="_increase_product">+</button>
                    <span class="_product_count">{{ $item->count }}</span>
                    <button class="_decrease_product">-</button>
                </div>

                <form method="POST" action="/cart/product/{{ $item->product->id }}">
                    @method('delete')
                    @csrf
                    <input type="submit" style="margin-left: 10px; color: red" value="delete">
                </form>

                <ul class="_errors"></ul>

            </div>

        @endforeach

        @if(count($cart->items))
            <div class="form">

                <label for="password">Пароль:
                    <input type="text" id="password">
                </label>

                <ul id="errors"></ul>

                <button id="new_order">Сформировать заказ</button>
            </div>
        @endif

    </div>
@endsection
