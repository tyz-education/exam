@extends('base')

@section('main')

    @parent

    <div class="form flex-column">

        <label class="form__label"><span>Имя</span>
            <input type="text" class="form__input" id="name">
        </label>

        <label class="form__label"><span>Фамииля</span>
            <input type="text" class="form__input" id="surname">
        </label>

        <label class="form__label"><span>Отчество</span>
            <input type="text" class="form__input" id="patronymic">
        </label>

        <label class="form__label"><span>Email</span>
            <input type="email" class="form__input" id="email">
        </label>

        <label class="form__label"><span>Логин</span>
            <input type="text" class="form__input" id="login">
        </label>

        <label class="form__label"><span>Пароль</span>
            <input type="password" class="form__input" id="password">
        </label>

        <label class="form__label"><span>Потверджение пароля</span>
            <input type="password" class="form__input" id="password_confirmation">
        </label>

        <label class="form__label"><span>Политика конфиденциальности</span>
            <input type="checkbox" class="form__checkbox" id="rules">
        </label>

        <ul id="errors"></ul>

        <button id="register_button">Регистрация</button>

    </div>

@endsection
