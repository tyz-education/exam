@extends('base')

@section('main')

    @parent

    <div class="product _product" data-id="{{ $product->id }}">
        <div class="product__table">Название: {{ $product->name }}</div>
        <div class="product__table">Цена: {{ $product->price }} руб.</div>
        <div class="product__table">Количество на складе: {{ $product->count }}</div>
        <div class="product__table">Страна изготовитель: {{ $product->country }}</div>
        <div class="product__table">Год изготовления: {{ $product->year }}</div>
        <div class="product__table">Модель: {{ $product->model }}</div>
        <div class="product__table">Категория: {{ $product->category->name }}</div>
        @auth
            @if (!auth()->user()->hasCartItem($product->id))
                <button class="_product_cart_add">В корзину</button>
            @else
                <a href="/cart">Уже в корзине</a>
            @endif
        @endauth
        <div class="product__image">
            <img src="{{ $product->photo }}" alt="{{ $product->name }}">
        </div>
    </div>

@endsection
