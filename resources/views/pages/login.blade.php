@extends('base')

@section('main')

    @parent

    <div class="form flex-column">

        <label class="form__label"><span>Логин</span>
            <input type="text" class="form__input" id="login">
        </label>

        <label class="form__label"><span>Пароль</span>
            <input type="password" class="form__input" id="password">
        </label>

        <ul id="errors"></ul>

        <button id="login_button">Вход</button>

    </div>
@endsection
