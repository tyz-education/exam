@extends('base')

@section('main')

    @parent

    <div class="products">

        @foreach($products as $product)

            <div class="product _product" data-id="{{ $product->id }}">
                <a href="/catalog/product/{{ $product->id }}">
                    <div class="product__name">{{ $product->name }}</div>
                    <div class="product__price">{{ $product->price }} руб.</div>
                    <div class="product__image">
                        <img src="{{ $product->photo }}" alt="{{ $product->name }}">
                    </div>
                </a>
                @auth
                    @if (!auth()->user()->hasCartItem($product->id))
                        <button class="_product_cart_add">В корзину</button>
                    @else
                        <a href="/cart">Перейти в корзину</a>
                    @endif
                @endauth
            </div>

        @endforeach

    </div>

@endsection
