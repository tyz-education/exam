@extends('admin')

@section('main')

    @parent

    @foreach($categories as $category)
        <div class="flex">
            <a href="/admin/categories/{{ $category->id }}">
                <b>{{ $category->id }} </b>{{ $category->name }}<br>
            </a>
            <form method="POST" action="/admin/categories/{{ $category->id }}">
                @method('delete')
                @csrf
                <input type="submit" style="margin-left: 10px; color: red" value="delete">
            </form>
        </div>
    @endforeach

    <a href="/admin/categories/new">Новая категория</a>

@endsection
