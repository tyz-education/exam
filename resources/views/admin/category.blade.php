@extends('admin')

@section('main')

    @parent

    <form action="/admin/categories/{{ $category->id ?: 'new' }}" method="POST" class="form flex-column">

        @if($category->id)
            @method('patch')
        @endif
        @csrf

        <label class="form__label"><span>Название</span>
            <input type="text" value="{{ $category->name }}" class="form__input" name="name">
        </label>

        <ul class="errors">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

        <input type="submit" value="{{ $category->id ? 'Обновить' : 'Создать' }}">

    </form>

@endsection
