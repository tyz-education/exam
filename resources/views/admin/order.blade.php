@extends('admin')

@section('main')

    @parent

    <form action="/admin/orders/{{ $order->id }}" method="POST" class="form flex-column">

        @method('patch')
        @csrf

        <label class="form__label"><span>Статус</span>
            <input type="text" value="{{ $order->status }}" class="form__input" name="status">
        </label>

        @foreach ($order->items as $item)
            <span>Заказано {{ $item->product->name }} {{ $item->count }}шт.</span>
        @endforeach

        <ul class="errors">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

        <input type="submit" value="Обновить">

    </form>

@endsection
