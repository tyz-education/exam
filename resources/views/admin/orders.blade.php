@extends('admin')

@section('main')

    @parent

    @foreach($orders as $order)
        <div class="flex">
            <a href="/admin/orders/{{ $order->id }}">
                Заказ №{{ $order->id }}, пользователя {{ $order->user->name }}
            </a>
            <form method="POST" action="/admin/orders/{{ $order->id }}">
                @method('delete')
                @csrf
                <input type="submit" style="margin-left: 10px; color: red" value="delete">
            </form>
        </div>
    @endforeach

@endsection
