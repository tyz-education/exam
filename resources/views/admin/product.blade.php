@extends('admin')

@section('main')

    @parent

    <form action="/admin/products/{{ $product->id ?: 'new' }}" method="POST" class="form flex-column" enctype="multipart/form-data">

        @if($product->id)
            @method('patch')
        @endif
        @csrf

        <label class="form__label"><span>Название</span>
            <input type="text" value="{{ $product->name }}" class="form__input" name="name">
        </label>

        <label class="form__label"><span>Цена</span>
            <input type="text" value="{{ $product->price }}" class="form__input" name="price">
        </label>

        <label class="form__label"><span>Количество</span>
            <input type="text" value="{{ $product->count }}" class="form__input" name="count">
        </label>

        <label class="form__label"><span>Страна</span>
            <input type="text" value="{{ $product->country }}" class="form__input" name="country">
        </label>

        <label class="form__label"><span>Год</span>
            <input type="date" value="{{ $product->year }}" class="form__input" name="year">
        </label>

        <label class="form__label"><span>Модель</span>
            <input type="text" value="{{ $product->model }}" class="form__input" name="model">
        </label>

        <label class="form__label"><span>Опубликовать</span>
            <input type="checkbox" {{ $product->publish ? 'checked' : '' }} class="form__input" name="publish">
        </label>

        <label class="form__label"><span>Категория</span>
            <select name="category_id">
                @if($product->category)
                    <option value="{{ $product->category->id }}">{{ $product->category->name }}</option>
                @endif
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </label>

        <label class="form__label"><span>Фото</span>
            @if($product->photo)
                <img src="{{ $product->photo }}" alt="photo">
            @endif
            <input type="file" class="form__input" name="photo">
        </label>

        <ul class="errors">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

        <input type="submit" value="{{ $product->id ? 'Обновить' : 'Создать' }}">

    </form>

@endsection
