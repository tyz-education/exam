@extends('admin')

@section('main')

    @parent

    @foreach($products as $product)
        <div class="flex">
            <a href="/admin/products/{{ $product->id }}">
                <b>{{ $product->id }} </b>{{ $product->name }}<br>
            </a>
            <form method="POST" action="/admin/products/{{ $product->id }}">
                @method('delete')
                @csrf
                <input type="submit" style="margin-left: 10px; color: red" value="delete">
            </form>
        </div>
    @endforeach

    <a href="/admin/products/new">Новый продукт</a>

@endsection
