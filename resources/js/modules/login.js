function init(){

    handleLoginButton()
}

function handleLoginButton(){

    const button = document.querySelector(`#login_button`)
    const errors = document.querySelector(`#errors`)

    if (!button) return

    button.addEventListener(`click`, async () => {

        try {

            const response = await window.axios.post('/login', getElementValues())

            location.href = '/'
        }
        catch (error){

            errors.innerHTML = ''
            Object.values(error.response.data.errors).forEach(value => {
                errors.innerHTML += `<li>${value}</li>`
            })
        }
    })
}

function getElementValues(){

    const login = document.querySelector(`#login`)
    const password = document.querySelector(`#password`)

    return {
        login: login?.value,
        password: password?.value,
    }
}

init()
