function init(){

    handleRegisterButton()
}

function handleRegisterButton(){

    const button = document.querySelector(`#register_button`)
    const errors = document.querySelector(`#errors`)

    if (!button) return

    button.addEventListener(`click`, async () => {

        try {

            const response = await window.axios.post('/register', getElementValues())

            location.href = '/'
        }
        catch (error){

            errors.innerHTML = ''
            Object.values(error.response.data.errors).forEach(value => {
                errors.innerHTML += `<li>${value}</li>`
            })
        }
    })
}

function getElementValues(){

    const name = document.querySelector(`#name`)
    const surname = document.querySelector(`#surname`)
    const patronymic = document.querySelector(`#patronymic`)
    const login = document.querySelector(`#login`)
    const email = document.querySelector(`#email`)
    const password = document.querySelector(`#password`)
    const password_confirmation = document.querySelector(`#password_confirmation`)
    const rules = document.querySelector(`#rules`)

    return {
        name: name?.value,
        surname: surname?.value,
        patronymic: patronymic?.value,
        login: login?.value,
        email: email?.value,
        password: password?.value,
        password_confirmation: password_confirmation?.value,
        rules: rules?.checked,
    }
}

init()
