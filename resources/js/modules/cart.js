function init(){

    handleCartButtons()
    handleIncreaseCartButtons()
    handleDecreaseCartButtons()
    handleNewOrder()
}

function handleCartButtons(){

    const buttons = document.querySelectorAll(`._product_cart_add`)

    if (buttons.length < 1) return

    buttons.forEach(button => {

        let product = button.closest(`._product`)
        let in_cart = false

        button.addEventListener(`click`, async () => {

            if (in_cart) return

            await window.axios.post(`/cart/product/${product.dataset.id}`)

            in_cart = true
            button.innerHTML = "Уже в корзине"
        })

    })
}

function handleIncreaseCartButtons(){

    const buttons = document.querySelectorAll(`._increase_product`)

    if (buttons.length < 1) return

    buttons.forEach(button => {

        let parent = button.closest(`._parent`)

        changeCartCount(button, parent, 'increase')

    })
}

function handleDecreaseCartButtons(){

    const buttons = document.querySelectorAll(`._decrease_product`)

    if (buttons.length < 1) return

    buttons.forEach(button => {

        let parent = button.closest(`._parent`)

        changeCartCount(button, parent, 'decrease')

    })
}

function changeCartCount(button, parent, method){

    button.addEventListener(`click`, async () => {

        const response = await window.axios.patch(`/cart/product/${method}/${parent.dataset.productId}`)
        const errors = parent.querySelector(`._errors`)

        parent.querySelector(`._product_count`).innerHTML = response.data.count
        errors.innerHTML = ""

        if (response.data.error){

            errors.innerHTML = `<li>${response.data.error}</li>`
        }

    })
}

function handleNewOrder(){

    const button = document.querySelector(`#new_order`)
    const password = document.querySelector(`#password`)
    const errors = document.querySelector(`#errors`)

    if (!button) return

    button.addEventListener(`click`, async () => {

        try{

            await window.axios.post(`/order/new`, {password: password.value})

            alert('Заказ успешно оформлен')

            location.href = '/'
        }
        catch (exception){

            errors.innerHTML = ""
            errors.innerHTML = exception.response.data.message
        }

    })
}

init()
